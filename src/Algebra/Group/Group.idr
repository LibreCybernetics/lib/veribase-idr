module Algebra.Group.Group

import Basic
import public Algebra.Group.Magma
import public Algebra.Group.Semigroup
import Algebra.Group.InverseSemigroup
import public Algebra.Group.Monoid

%default total
%access public export

--
-- Interface
--

interface Magma a => Group a where
  e : a
  neg : a -> a
  export opAssoc : (x, y, z: a) -> x <> (y <> z) = (x <> y) <> z
  export eLeft  : (x: a) -> e <> x = x
  export eRight : (x: a) -> x <> e = x
  export negLeft  : (x: a) -> neg x <> x = e
  export negRight : (x: a) -> x <> neg x = e

--
-- Derivations
--

%overlapping
[semigroupFromGroup] Group a => Semigroup a where
  opAssoc = Algebra.Group.Group.opAssoc

%overlapping
[inverseSemigroupFromGroup] Group a => InverseSemigroup a using semigroupFromGroup where
  neg = Algebra.Group.Group.neg
  negInner x =
    rewrite negRight x in
    rewrite eLeft x in
    Refl
  negOuter x =
    rewrite negLeft x in
    rewrite eLeft (Algebra.Group.Group.neg x) in
    Refl

%overlapping
[monoidFromGroup] Group a => Monoid a using semigroupFromGroup where
  e = Algebra.Group.Group.e
  eLeft  = Algebra.Group.Group.eLeft
  eRight = Algebra.Group.Group.eRight
