module Algebra.Group.InverseSemigroup

import Algebra.Group.Magma
import Algebra.Group.Semigroup

%default total
%access public export

--
-- Interface
--

interface Semigroup a => InverseSemigroup a where
  neg : a -> a
  export negInner : (x: a) -> x = x <> neg x <> x
  export negOuter : (x: a) -> neg x = neg x <> x <> neg x
