module Algebra.Group.Monoid

import Algebra.Group.Magma
import Algebra.Group.Semigroup

%default total
%access public export

--
-- Interface
--

interface Semigroup a => Monoid a where
  e : a
  export eLeft  : (x: a) -> e <> x = x
  export eRight : (x: a) -> x <> e = x
