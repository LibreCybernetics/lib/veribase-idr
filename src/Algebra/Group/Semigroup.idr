module Algebra.Group.Semigroup

import Algebra.Group.Magma

%default total
%access public export

--
-- Interface
--

interface Magma a => Semigroup a where
  export opAssoc : (x, y, z: a) -> x <> (y <> z) = (x <> y) <> z
