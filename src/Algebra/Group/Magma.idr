module Algebra.Group.Magma

%default total
%access public export

--
-- Interface
--

infixl 6 <>

interface Magma a where
  (<>) : a -> a -> a
