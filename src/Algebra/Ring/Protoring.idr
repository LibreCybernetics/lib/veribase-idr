module Algebra.Ring.Protoring

import Builtins

%default total
%access public export

--
-- Interface
--

infixl 8 +
infixl 9 *

||| This is the prototype for veribase rings, it is the commonality between
||| Nearrings, Nonassociative rings, and Semirings. Monoid (a, +), Magma (a, *),
||| and no distributions. (This interface isn't useful beyond multiple
||| inheritance)
interface Protoring a where
  (+) : a -> a -> a
  (*) : a -> a -> a
  zero : a
  export sumAssoc  : (x, y, z: a) -> x + (y + z) = (x + y) + z
  export zeroLeft  : (x: a) -> zero + x = x
  export zeroRight : (x: a) -> x + zero = x

--
-- Type-level Checks
--

NonZero : Protoring a => a -> Type
NonZero x = (x = zero) -> Void
