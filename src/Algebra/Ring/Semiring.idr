module Algebra.Ring.Semiring

import public Algebra.Ring.Protoring

%default total
%access public export

--
-- Interface
--

interface Protoring a => Semiring a where
  one : a
  export sumComm : (x, y: a) -> x + y = y + x
  export prodAssoc : (x, y, z: a) -> x * (y * z) = (x * y) * z
  export oneLeft  : (x: a) -> one * x = x
  export oneRight : (x: a) -> x * one = x
  export prodDistSumLeft  : (x, y, z: a) -> x * (y + z) = (x * y) + (x * z)
  export prodDistSumRight : (x, y, z: a) -> (x + y) * z = (x * z) + (y * z)
  export zeroAnniLeft  : (x: a) -> Algebra.Ring.Protoring.zero * x = Algebra.Ring.Protoring.zero
  export zeroAnniRight : (x: a) -> x * Algebra.Ring.Protoring.zero = Algebra.Ring.Protoring.zero
