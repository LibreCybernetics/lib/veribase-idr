module Algebra.Ring.Ring

import Builtins
import public Algebra.Ring.Protoring
import public Algebra.Ring.Semiring

%default total
%access public export

infixl 8 -

--
-- Interfaces
--

interface Protoring a => Ring a where
  ||| The Integers are an initial object in the category of rings
  fromInteger : Integer -> a
  (-) : a -> a -> a
  neg : a -> a
  one  : a

  export negLeft : (x: a) -> neg x + x = Algebra.Ring.Protoring.zero
  export negRight : (x: a) -> x + neg x = Algebra.Ring.Protoring.zero
  export sumComm : (x, y: a) -> x + y = y + x

  export prodAssoc : (x, y, z: a) -> x * (y * z) = (x * y) * z
  export oneLeft : (x: a) -> one * x = x
  export oneRight : (x: a) -> x * one = x

  export prodDistSumLeft  : (x, y, z: a) -> x * (y + z) = (x * y) + (x * z)
  export prodDistSumRight : (x, y, z: a) -> (x + y) * z = (x * z) + (y * z)

%overlapping
[fromRing] Ring a => Semiring a where
  one = Algebra.Ring.Ring.one

  sumComm = Algebra.Ring.Ring.sumComm

  prodAssoc = Algebra.Ring.Ring.prodAssoc
  oneLeft   = Algebra.Ring.Ring.oneLeft
  oneRight  = Algebra.Ring.Ring.oneRight
  prodDistSumLeft  = Algebra.Ring.Ring.prodDistSumLeft
  prodDistSumRight = Algebra.Ring.Ring.prodDistSumRight
  zeroAnniLeft  = believe_me () -- TODO
  zeroAnniRight = believe_me () -- TODO
