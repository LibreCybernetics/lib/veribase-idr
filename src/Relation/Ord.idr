module Relation.Ord

import Basic
import public Data.Bool.Base
import public Data.Either.Base
import public Relation.PartialOrd
import Relation.Eq

%default total
%access public export

data Ordering = LT | EQ | GT

Eq Ordering where
  LT == LT = True
  EQ == EQ = True
  GT == GT = True
  _  == _  = False

  eqRefl LT = Refl
  eqRefl EQ = Refl
  eqRefl GT = Refl

  eqSym LT LT Refl = Refl
  eqSym LT EQ Refl impossible
  eqSym LT GT Refl impossible
  eqSym EQ LT Refl impossible
  eqSym EQ EQ Refl = Refl
  eqSym EQ GT Refl impossible
  eqSym GT LT Refl impossible
  eqSym GT EQ Refl impossible
  eqSym GT GT Refl = Refl

  eqTran LT LT LT Refl Refl = Refl
  eqTran _  LT EQ Refl Refl impossible
  eqTran _  LT GT Refl Refl impossible
  eqTran LT EQ _  Refl Refl impossible
  eqTran LT GT _  Refl Refl impossible
  eqTran EQ LT _  Refl Refl impossible
  eqTran EQ EQ EQ Refl Refl = Refl
  eqTran GT LT _  Refl Refl impossible
  eqTran GT EQ _  Refl Refl impossible
  eqTran _  GT LT Refl Refl impossible
  eqTran _  GT EQ Refl Refl impossible
  eqTran GT GT GT Refl Refl = Refl

interface PartialOrd a => Ord a where
  ordTotal : (x, y: a) -> Either (x <= y = True) (x >= y = True)

compare : Ord a => a -> a -> Ordering
compare x y = if x < y  then LT else
              if x == y then EQ else GT

Ord () where
  ordTotal () () = Left Refl
