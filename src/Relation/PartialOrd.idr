module Relation.PartialOrd

import Basic
import public Data.Bool.Base
import public Relation.PreOrd
import Relation.Eq

%default total
%access public export

infix 6 <, >

interface (PreOrd a, Eq a) => PartialOrd a where
  (<) : a -> a -> Bool
  export ordAnti : (x, y: a) -> x <= y = True -> x >= y = True -> x == y = True
  export strictOrd : (x, y: a) -> x < y && y < x = False

(>) : PartialOrd a => a -> a -> Bool
(>) = flip (<)

PartialOrd () where
  () < () = False
  ordAnti () () Refl Refl = Refl
  strictOrd () () = Refl
