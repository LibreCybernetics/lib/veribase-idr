module Relation.PreOrd

import Basic
import public Data.Bool.Base

%default total
%access public export

infix 6 <=, >=

interface PreOrd a where
   (<=) : a -> a -> Bool
   export ordRefl : (x: a) -> x <= x = True
   export ordTran : (x, y, z: a) -> x <= y = True -> y <= z = True
                  -> x <= z = True

(>=) : PreOrd a => a -> a -> Bool
(>=) = flip (<=)

PreOrd () where
  () <= () = True
  ordRefl () = Refl
  ordTran () () () Refl Refl = Refl
