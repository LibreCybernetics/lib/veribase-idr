module Relation.Eq

import Basic
import public Data.Bool.Base

%default total
%access public export

infix 6 ==, /=

--
-- Interface
--

interface Eq a where
  (==) : a -> a -> Bool

  eqRefl : (x: a) -> x == x = True
  eqSym  : (x, y: a) -> x == y = True -> y == x = True
  eqTran : (x, y, z: a) -> x == y = True -> y == z = True -> x == z = True

(/=) : Eq a => a -> a -> Bool
x /= y = not $ x == y

--
-- Dependencies' Interface Implementations
--

Eq () where
  () == () = True

  eqRefl () = Refl
  eqSym  () () Refl = Refl
  eqTran () () () Refl Refl = Refl
