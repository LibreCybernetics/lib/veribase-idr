||| Basic definitions that don't depend on any type
|||
||| Copyright  : LibreCybernetics
||| License    : Parity 5.0.0 or Prosperity 2.0.0
||| Maintainer : Fabián Heredia Montiel
||| Stability  : Stable
module Basic

import public Builtins

%default total
%access public export

infixr 9  .
infixr 0  $
infixl 0  &

fst : (a, b) -> a
fst (x, _) = x

snd : (a, b) -> b
snd (_, y) = y

identity : a -> a
identity x = x

||| Constant function
||| @ c Constant value
const : (c: a) -> b -> a
const c _ = c

||| Coerces a specified type
|||
||| ```idris example
||| the Nat 10
||| ```
||| @ a The type
||| @ x The value
the : (a: Type) -> (x: a) -> a
the _ = identity

||| See: https://wiki.haskell.org/Currying
curry : (f: (a, b) -> c) -> (a -> b -> c)
curry f x y = f (x, y)

||| See: https://wiki.haskell.org/Currying
uncurry : (f: a -> b -> c) -> (a, b) -> c
uncurry f (x, y) = f x y

||| Flip the arguments of a function
flip : (f : a -> b -> c) -> b -> a -> c
flip f x y = f y x

||| Function application
($) : (a -> b) -> a -> b
f $ x = f x

||| Pipe operator (Function application fliped)
(&) : a -> (a -> b) -> b
(&) = flip ($)

||| Function composition (f ∘ g)
||| @ f after
||| @ g first
(.) : (f: b -> c) -> (g: a -> b) -> (a -> c)
(.) f g x = f (g x)

--
-- Dependent Types Decidability
--

||| Type negation since Void is uninhabited
||| Warning: Not (Not a) ≠ a, see:
||| https://www.wikiwand.com/en/Double_negation#/Double_negative_elimination
Not : Type -> Type
Not a = a -> Void

||| Interface for all uninhabited types `a` along with counterproof (`Not a`)
interface Uninhabited a where
  uninhabited : Not a

Uninhabited Void where
  uninhabited = void

||| Proof by contradiction, from an uninhabited type anything follows.
||| (void : Void -> b) . (uninhabited : a -> Void)
absurd : Uninhabited a => a -> b
absurd = void . uninhabited

||| Decidable Types that for any value-type provide either:
data Dec : Type -> Type where
  ||| Proof
  Yes : (    p) -> Dec p
  ||| Counterproof
  No  : (Not p) -> Dec p
