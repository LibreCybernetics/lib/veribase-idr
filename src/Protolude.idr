||| Will import bases, interfaces and casts
module Protolude

import public Builtins
import public IO
import public Basic

import public Algebra.Group.Magma
import public Algebra.Group.Semigroup
import public Algebra.Group.Monoid
import public Algebra.Group.Group
import public Algebra.Ring.Semiring
import public Algebra.Ring.Ring

import public Category.Applicative
import public Category.Functor
import public Category.Bifunctor

import public Data.Bool
import public Data.Cast
import public Data.Either
import public Data.List
import public Data.Maybe
import public Data.Nat

import public Relation.Eq
import public Relation.Ord
import public Relation.PartialOrd
import public Relation.PreOrd
