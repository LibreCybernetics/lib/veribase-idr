module Category.Applicative

import Basic
import Category.Functor

%default total
%access public export

infixl 3 <*>

interface Functor f => Applicative (f: Type -> Type) where
  (<*>) : f (a -> b) -> f a -> f b
  pure : a -> f a
  export appId : (x: f a) -> pure Basic.identity <*> x = x
  export appComp : (g: f (b -> c)) -> (h: f (a -> b)) -> (x: f a)
                 -> pure (.) <*> g <*> h <*> x = g <*> (h <*> x)
  export appHomo : (g: a -> b) -> (x: a)
                 -> pure g <*> pure x = pure (g x)
  export appInter : (g: f (a -> b)) -> (x: a)
                  -> g <*> pure x = pure ($ x) <*> g
