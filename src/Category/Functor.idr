module Category.Functor

import Basic

%default total
%access public export

infixl 4 <$>
infixr 4 <&>

interface Functor (f: Type -> Type) where
   (<$>) : (a -> b) -> f a -> f b
   export funId : (x: f a) -> Basic.identity <$> x = x
   export funComp : (g: b -> c) -> (h: a -> b) -> (x: f a)
                      -> g . h <$> x = (g <$>) . (h <$>) $ x

(<&>) : (Functor f) => f a -> (a -> b) -> f b
(<&>) = flip (<$>)
