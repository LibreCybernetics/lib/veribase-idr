module Category.Monad

import public Category.Applicative

%default total
%access public export

infixl 1 >>=

interface Applicative m => Monad (m: Type -> Type) where
  (>>=) : m a -> (a -> m b) -> m b

  export monadIdLeft  : (x: a) -> (f: a -> m b) -> Category.Applicative.pure x >>= f = f x
  export monadIdRight : (x: m a) -> x >>= Category.Applicative.pure = x
  export monadAssoc : (x: m a) -> (f: a -> m b) -> (g: b -> m c)
                    -> x >>= (\x => f x >>= g) = (x >>= f) >>= g
