module Category.Biunctor

import Basic
import Category.Functor

%default total
%access public export

infixl 4 <$, $>
infixr 4 <&, &>

interface Bifunctor (f: Type -> Type -> Type) where
  (<$) : (a -> b) -> f a c -> f b c
  ($>) : (a -> b) -> f c a -> f c b
  export funIdLeft  : (x: f a b) -> Basic.identity <$ x = x
  export funIdRight : (x: f a b) -> Basic.identity $> x = x
  export funCompLeft  : (g: b -> c) -> (h: a -> b) -> (x: f a d)
                      -> g . h <$ x = (g <$) . (h <$) $ x
  export funCompRight : (g: b -> c) -> (h: a -> b) -> (x: f d a)
                      -> g . h $> x = (g $>) . (h $>) $ x

(<&) : Bifunctor f => f a c -> (a -> b) -> f b c
(<&) = flip (<$)

(&>) : Bifunctor f => f c a -> (a -> b) -> f c b
(&>) = flip ($>)

bimap : Bifunctor f => (a -> c) -> (b -> d) -> f a b -> f c d
bimap f g = (f <$) . (g $>)

-- TODO: Unification problem, https://github.com/idris-lang/Idris-dev/issues/2576#issuecomment-451606962
-- Bifunctor f => Functor (f a) where
--   (<$>) = ($>)
--   funId = funIdRight
--   funComp = funCompRight
