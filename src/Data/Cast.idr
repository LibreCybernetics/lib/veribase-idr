module Data.Cast

import Basic

%default total
%access public export

--
-- Interface
--

interface Cast a b where
  cast : a -> b

(Cast a b, Cast c d) => Cast (a, c) (b, d) where
  cast (x, y) = (cast x, cast y)
