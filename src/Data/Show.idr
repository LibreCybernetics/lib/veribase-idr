module Data.Show

import Data.Cast

%default total
%access public export

show : Cast a String => a -> String
show = cast
