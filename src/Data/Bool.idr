||| Copyright  : LibreCybernetics
||| License    : Parity 5.0.0 or Prosperity 2.0.0
||| Maintainer : Fabián Heredia Montiel
||| Stability  : Stable
module Data.Bool

import public Data.Bool.Base
import public Data.Bool.Cast
import public Data.Bool.Interfaces
import public Data.Bool.ShowRead
