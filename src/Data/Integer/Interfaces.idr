module Data.Integer.Interfaces

import Basic
import Data.Bool.Cast
import public Algebra.Ring.Ring
import public Relation.Eq
import public Relation.Ord

%default total
%access public export

Protoring Integer where
  (+) = prim__addBigInt
  (*) = prim__mulBigInt
  zero = 0

  sumAssoc  = believe_me ()
  zeroLeft  = believe_me ()
  zeroRight = believe_me ()

Ring Integer where
  fromInteger = identity
  (-) = prim__subBigInt
  neg = (zero `prim__subBigInt`)
  one = 1
  negLeft   = believe_me ()
  negRight  = believe_me ()
  sumComm   = believe_me ()
  prodAssoc = believe_me ()
  oneLeft   = believe_me ()
  oneRight  = believe_me ()
  prodDistSumLeft  = believe_me ()
  prodDistSumRight = believe_me ()

Eq Integer where
  x == y = cast $ prim__eqBigInt x y

  eqRefl = believe_me ()
  eqSym  = believe_me ()
  eqTran = believe_me ()

PreOrd Integer where
  x <= y = cast $ prim__lteBigInt x y

  ordRefl = believe_me ()
  ordTran = believe_me ()

PartialOrd Integer where
  x < y = cast $ prim__ltBigInt x y
  ordAnti   = believe_me ()
  strictOrd = believe_me ()

Ord Integer where
  ordTotal = believe_me ()
