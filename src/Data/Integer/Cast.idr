module Data.Integer.Cast

import public Data.Cast

%default total
%access public export

Cast Integer String where
  cast = prim__toStrBigInt
