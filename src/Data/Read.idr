module Data.Read

import Data.Cast
import Data.Either.Base

read : Cast String (Either e d) => String -> Either e d
read = cast
