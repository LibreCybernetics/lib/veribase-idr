module Data.Either.Interfaces

import Basic
import public Data.Either.Base
import Relation.Eq
import Category.Bifunctor

%default total
%access public export

(Eq a, Eq b) => Eq (Either a b) where
  (Left  x) == (Left  y) = x == y
  (Right x) == (Right y) = x == y
  _         == _         = False

  eqRefl (Left  x) = eqRefl x
  eqRefl (Right x) = eqRefl x

  eqSym (Left  x) (Left  y) p   = eqSym x y p
  eqSym (Right x) (Right y) p   = eqSym x y p
  eqSym (Left  _) (Right _) Refl impossible
  eqSym (Right _) (Left  _) Refl impossible

  eqTran (Left  x) (Left  y) (Left  z) p1  p2  = eqTran x y z p1 p2
  eqTran (Left  _) (Left  _) (Right _) Refl Refl impossible
  eqTran (Left  _) (Right _) (_      ) Refl Refl impossible
  eqTran (Right _) (Left  _) (_      ) Refl Refl impossible
  eqTran (Right _) (Right _) (Left  _) Refl Refl impossible
  eqTran (Right x) (Right y) (Right z) p1  p2  = eqTran x y z p1 p2

Bifunctor Either where
  f <$ (Left  x) = Left  $ f x
  _ <$ (Right x) = Right     x
  _ $> (Left  x) = Left      x
  f $> (Right x) = Right $ f x

  funIdLeft  (Left  _) = Refl
  funIdLeft  (Right _) = Refl
  funIdRight (Left  _) = Refl
  funIdRight (Right _) = Refl
  funCompLeft  f g (Left  _) = Refl
  funCompLeft  _ _ (Right _) = Refl
  funCompRight _ _ (Left  _) = Refl
  funCompRight f g (Right _) = Refl
