module Data.Either.Base

import Basic
import Data.Bool.Cast

%default total
%access public export

--
-- Type
--

||| Given two types A and B, it gives the sum, or coproduct, A ∐ B. See:
||| https://www.wikiwand.com/en/Tagged_union and
||| https://www.wikiwand.com/en/Coproduct
data Either a b = Left  a
                | Right b

--
-- Type-level Checks
--

||| Type-level checks iff Left
data IsLeft : Either a b -> Type where
  ItIsLeft : IsLeft (Left _)

Uninhabited (IsLeft (Right _)) where
  uninhabited ItIsLeft impossible

||| Type-level checks iff Right
data IsRight : Either a b -> Type where
  ItIsRight : IsRight (Right _)

Uninhabited (IsRight (Left _)) where
  uninhabited ItIsRight impossible

--
-- Decidability of Type-level Checks
--

isItLeft : (e: Either a b) -> Dec (IsLeft e)
isItLeft (Left  _) = Yes ItIsLeft
isItLeft (Right _) = No absurd

isItRight : (e: Either a b) -> Dec (IsRight e)
isItRight (Left  _) = No absurd
isItRight (Right _) = Yes ItIsRight

--
-- Value-level Checks
--

isLeft : Either a b -> Bool
isLeft e = isItLeft e & cast

isRight : Either a b -> Bool
isRight e = isItRight e & cast

--
-- Utility Functions
--

||| Extracts a value from a Left
fromLeft : (e: Either a b) -> {auto ok: IsLeft e} -> a
fromLeft (Left x)  = x
fromLeft (Right _) impossible

||| Extracts a value from a Right
fromRight : (e: Either a b) -> {auto ok: IsRight e} -> b
fromRight (Right x) = x
fromRight (Left _)  impossible

||| Swaps A ∐ B for B ∐ A
swap : Either a b -> Either b a
swap (Left  x) = Right x
swap (Right x) = Left  x
