module Data.Either.Properties

import public Data.Either.Base

%default total
%access export

||| swap is an involution
swapInvolution : (e: Either a b) -> swap (swap e) = e
swapInvolution (Left  _) = Refl
swapInvolution (Right _) = Refl

||| fromLeft . Left = id
fromLeftInverseLeft : (x: a) -> fromRight (Right x) = x
fromLeftInverseLeft _ = Refl

||| Left . fromLeft = id
leftInverseFromLeft : (e: Either a b) -> {auto ok: IsLeft e} -> Left (fromLeft e) = e
leftInverseFromLeft (Left  _) = Refl
leftInverseFromLeft (Right _) impossible

||| fromRight . Right = id
fromRightLeftInverseRight : (x: a) -> fromRight (Right x) = x
fromRightLeftInverseRight _ = Refl

||| Left . fromLeft = id
rightLeftInverseFromRight : (e: Either a b) -> {auto ok: IsRight e} -> Right (fromRight e) = e
rightLeftInverseFromRight (Left  _) impossible
rightLeftInverseFromRight (Right _) = Refl
