module Data.Either.Cast

import Basic
import public Data.Cast
import public Data.Either.Base
import Data.Maybe.Base

%default total
%access public export

--
-- Dec p is Equivalente to Either p (Not p)
--

Cast (Dec p) (Either p (Not p)) where
  cast (Yes p) = Left   p
  cast (No cp) = Right cp

Cast (Either p (Not p)) (Dec p) where
  cast (Left   p) = Yes p
  cast (Right cp) = No cp

--
-- Flatten a sum of the same type
--

Cast (Either a a) a where
  cast (Left  x) = x
  cast (Right x) = x

--
-- Forget one type
--

Cast (Either a b) (Maybe a) where
  cast (Left  x) = Just x
  cast (Right _) = Nothing

Cast (Either a b) (Maybe b) where
  cast (Left  _) = Nothing
  cast (Right x) = Just x
