module Data.Nat.Properties

import Basic
import Data.Nat.Base
import Data.Bool.Cast

%default total
%access export

--
-- LTE
--

--

lteRefl : (x: Nat) -> x `LTE` x
lteRefl    Z  = ZIsLTE
lteRefl (S n) = SIsLTE $ lteRefl n

isItLTERefl : (x: Nat) -> isItLTE x x = (Yes (lteRefl x))
isItLTERefl    Z  = Refl
isItLTERefl (S n) = rewrite isItLTERefl n in Refl

--
-- Sum
--

-- 1

sumZero : (x: Nat) -> x `sum` Z = x
sumZero (  Z) = Refl
sumZero (S n) = rewrite sumZero n in Refl

-- 2

sumAlternative : (x, y: Nat) -> x `sum` (S y) = S (x `sum` y)
sumAlternative (  Z) _ = Refl
sumAlternative (S n) m = rewrite sumAlternative n m in Refl

sumComm : (x, y: Nat) -> x `sum` y = y `sum` x
sumComm (  Z) m = rewrite sumZero m in Refl
sumComm (S n) m =
  rewrite sumAlternative m n in
  rewrite sumComm n m in
  Refl

sumGTELeft : (x, y: Nat) -> (x `sum` y) `GTE` x
sumGTELeft    Z  m = ZIsLTE
sumGTELeft (S n) m = SIsLTE $ sumGTELeft n m

sumGTERight : (x, y: Nat) -> (x `sum` y) `GTE` y
sumGTERight n m = rewrite sumComm n m in sumGTELeft m n

-- 3

sumAssoc : (x, y, z: Nat) -> x `sum` (y `sum` z) = (x `sum` y) `sum` z
sumAssoc (  Z) (  _) (  _) = Refl
sumAssoc (S n) (  m) (  l) = rewrite sumAssoc n m l in Refl

--
-- Sum/Prod
--

-- 3

prodDistSumLeft : (x, y, z: Nat) -> x `prod` (y `sum` z) = (x `prod` y) `sum` (x `prod` z)
prodDistSumLeft (  Z) _ _ = Refl
prodDistSumLeft (S n) m l =
  rewrite prodDistSumLeft n m l in
  rewrite sumAssoc (m `sum` (n `prod` m)) l (n `prod` l) in
  rewrite sumComm (m `sum` (n `prod` m)) l in
  rewrite sumAssoc l m (n `prod` m) in
  rewrite sumComm l m in
  rewrite sumAssoc (sum m l) (prod n m) (prod n l) in
  Refl

prodDistSumRight : (x, y, z: Nat) -> (x `sum` y) `prod` z = (x `prod` z) `sum` (y `prod` z)
prodDistSumRight (  Z) _ _ = Refl
prodDistSumRight (S n) m l =
  rewrite prodDistSumRight n m l in
  rewrite sumAssoc l (n `prod` l) (m `prod` l) in Refl

--
-- Prod
--

-- 1

prodZero : (x: Nat) -> x `prod` Z = Z
prodZero (  Z) = Refl
prodZero (S n) = rewrite prodZero n in Refl

prodOne : (x: Nat) -> x `prod` (S Z) = x
prodOne (  Z) = Refl
prodOne (S n) = rewrite prodOne n in Refl


-- 2

prodAlternative : (x, y: Nat) -> x `prod` (S y) = x `sum` (x `prod` y)
prodAlternative (  Z) m = Refl
prodAlternative (S n) m =
  rewrite prodAlternative n m in
  rewrite sumAssoc n m (n `prod` m) in
  rewrite sumAssoc m n (n `prod` m) in
  rewrite sumComm n m in
  Refl

prodComm : (x, y: Nat) -> x `prod` y = y `prod` x
prodComm (  Z) m = rewrite prodZero m in Refl
prodComm (S n) m =
  rewrite prodAlternative m n in
  rewrite prodComm n m in
  Refl

-- 3

prodAssoc : (x, y, z: Nat) -> x `prod` (y `prod` z) = (x `prod` y) `prod` z
prodAssoc (  Z) _ _ = Refl
prodAssoc (S n) m l =
  rewrite prodAssoc n m l in
  rewrite prodDistSumRight m (n `prod` m) l in
  Refl
