module Data.Nat.Cast

import Basic
import public Data.Cast
import public Data.Nat.Base
import Data.Integer.Interfaces
import Data.Integer.Cast

%default total
%access public export

Cast Nat Integer where
  cast Z = 0
  cast (S (S (S (S (S (S (S (S (S (S n)))))))))) = 10 + cast n
  cast (S n) = 1 + cast n

Cast Nat String where
  cast n = cast $ the Integer (cast n)
