module Data.Nat.Extra

import Basic
import public Data.Nat.Base
import Data.Nat.Properties
import Data.Either.Base

%default total
%access public export

gcd : (x, y: Nat) -> {auto ok: Either (NonZero x) (NonZero y)} -> Nat
gcd Z Z {ok=(Left  _)} impossible
gcd Z Z {ok=(Right _)} impossible
gcd n Z = n
gcd Z m = m     -- We need to do a double step to ensure totality.
gcd n (S m) with (n `mod` (S m))
  | Z = (S m)  -- l < m
  | (S l) with ((S m) `mod` (S l))
    | Z = (S l)  -- k < l < m
    | (S k) = gcd (assert_smaller n (S l)) (assert_smaller (S m) (S k))

gcdNonZero : (x, y: Nat) -> {auto ok: Either (NonZero x) (NonZero y)} -> NonZero (gcd x y)
gcdNonZero Z Z {ok=(Left  _)} impossible
gcdNonZero Z Z {ok=(Right _)} impossible
gcdNonZero (S _) Z = IsNonZero
gcdNonZero Z (S _) = IsNonZero
gcdNonZero (S n) (S m) with ((S n) `mod` (S m))
  | Z = IsNonZero
  | (S l) with ((S m) `mod` (S l))
    |    Z  = IsNonZero
    | (S k) = ?hole -- TODO

lcm : (x, y: Nat) -> {auto ok: (NonZero x, NonZero y)} -> Nat
lcm Z _ {ok=(IsNonZero, _)} impossible
lcm _ Z {ok=(_, IsNonZero)} impossible
lcm n (S m) with (gcd n (S m))
  | Z = Z -- impossible: TODO See above
  | (S d) = (n `prod` m) `div` (S d)
