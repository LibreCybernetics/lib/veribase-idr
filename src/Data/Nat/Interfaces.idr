module Data.Nat.Interfaces

import Basic
import Data.Nat.Base
import Data.Nat.Properties
import Data.Bool.Cast

import Relation.Eq
import Relation.Ord
import Algebra.Ring.Protoring
import Algebra.Ring.Semiring

%default total
%access public export

Eq Nat where
  (  Z) == (  Z) = True
  (S n) == (S m) = n == m
  (  _) == (  _) = False

  eqRefl (  Z) = Refl
  eqRefl (S n) = eqRefl n

  eqSym (  Z) (  Z) Refl = Refl
  eqSym (S n) (S m) p    = eqSym n m p
  eqSym (  Z) (S _) Refl impossible
  eqSym (S _) (  Z) Refl impossible

  eqTran (  Z) (  Z) (  Z) Refl Refl = Refl
  eqTran (  Z) (  Z) (S _) Refl Refl impossible
  eqTran (  Z) (S _) (  _) Refl Refl impossible
  eqTran (S _) (  Z) (  _) Refl Refl impossible
  eqTran (S _) (S _) (  Z) Refl Refl impossible
  eqTran (S n) (S m) (S l) p1  p2  = eqTran n m l p1 p2

PreOrd Nat where
  (<=) = lte

  ordRefl    Z  = Refl
  ordRefl (S n) = rewrite ordRefl n in Refl

  ordTran    Z     Z     Z  Refl Refl = Refl
  ordTran    Z     Z  (S l) p1   p2   = ordTran Z Z l p1 p2
  ordTran    _  (S n)    Z  Refl Refl impossible
  ordTran    Z  (S m) (S l) p1   p2   = ordTran Z m l p1 p2
  ordTran (S _)    Z     _  Refl Refl impossible
  ordTran (S n) (S m) (S l) p1   p2   = ordTran n m l p1 p2

PartialOrd Nat where
  (<) = lt

  ordAnti    Z     Z  Refl Refl = Refl
  ordAnti    Z  (S _) Refl Refl impossible
  ordAnti (S _)    Z  Refl Refl impossible
  ordAnti (S n) (S m) p1 p2 = ordAnti n m p1 p2

  strictOrd    Z     Z  = Refl
  strictOrd    Z  (S _) = Refl
  strictOrd (S _)    Z  = Refl
  strictOrd (S n) (S m) = strictOrd n m

Protoring Nat where
  (+) = sum
  (*) = prod
  zero = Z

  sumAssoc = Data.Nat.Properties.sumAssoc
  zeroLeft _ = Refl
  zeroRight = sumZero

Semiring Nat where
  one = S Z

  sumComm = Data.Nat.Properties.sumComm

  prodAssoc = Data.Nat.Properties.prodAssoc
  oneLeft  n = rewrite sumZero n in Refl
  oneRight n = rewrite prodOne n in Refl

  prodDistSumLeft = Data.Nat.Properties.prodDistSumLeft
  prodDistSumRight = Data.Nat.Properties.prodDistSumRight

  zeroAnniLeft  _ = Refl
  zeroAnniRight n = rewrite prodZero n in Refl
