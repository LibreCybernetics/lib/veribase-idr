module Data.Nat.Base

import Basic
import Data.Bool.Base
import Data.Bool.Cast
import Data.Either.Base
import Data.Integer.Interfaces

%default total
%access public export

--
-- Type
--

data Nat = Z
         | S Nat

%name Nat n, m, l, k, j, i

--
-- Type-level checks
--

-- Zero or NonZero

||| Type-checks iff zero
data Zero : Nat -> Type where
  IsZero : Zero (Z)

Uninhabited (Zero (S _)) where
  uninhabited IsZero impossible

||| Type-checks iff greater than zero
data NonZero : Nat -> Type where
  IsNonZero : NonZero (S _)

Uninhabited (NonZero Z) where
  uninhabited IsNonZero impossible

-- Comparing Two Nats

||| Type-checks iff `x` < `y`
data LT : (x, y: Nat) -> Type where
  ZIsLT : Z `LT` (S _)
  SIsLT : n `LT` m -> (S n) `LT` (S m)

Uninhabited (Z `LT` Z) where
  uninhabited ZIsLT impossible
  uninhabited SIsLT impossible

Uninhabited ((S _) `LT` Z) where
  uninhabited ZIsLT impossible
  uninhabited SIsLT impossible

Uninhabited (n `LT` m) => Uninhabited ((S n) `LT` (S m)) where
  uninhabited  ZIsLT    impossible
  uninhabited (SIsLT p) = uninhabited p

||| Type-checks iff `x` ≤ `y`
data LTE : (x, y: Nat) -> Type where
  ZIsLTE : Z `LTE` _
  SIsLTE : n `LTE` m -> (S n) `LTE` (S m)

Uninhabited ((S _) `LTE` Z) where
  uninhabited ZIsLTE impossible
  uninhabited SIsLTE impossible

Uninhabited (n `LTE` m) => Uninhabited ((S n) `LTE` (S m)) where
  uninhabited ZIsLTE impossible
  uninhabited (SIsLTE p) = uninhabited p

GT : Nat -> Nat -> Type
GT = flip LT

GTE : Nat -> Nat -> Type
GTE = flip LTE

--
-- Decidibility of Type-level checks
--

isItZero : (n: Nat) -> Dec (Zero n)
isItZero (  Z) = Yes IsZero
isItZero (S _) = No  absurd

isItNonZero : (n: Nat) -> Dec (NonZero n)
isItNonZero (  Z) = No  absurd
isItNonZero (S _) = Yes IsNonZero

fromSIsLT : (S n) `LT` (S m) -> n `LT` m
fromSIsLT  ZIsLT    impossible
fromSIsLT (SIsLT p) = p

isItLT : (n, m: Nat) -> Dec (n `LT` m)
isItLT (  Z) (  Z) = No  absurd
isItLT (  Z) (S _) = Yes ZIsLT
isItLT (S _) (  Z) = No  absurd
isItLT (S n) (S m) with (isItLT n m)
  | Yes p = Yes (SIsLT p)
  | No cp = No  (cp . fromSIsLT)

fromSIsLTE : (S n) `LTE` (S m) -> n `LTE` m
fromSIsLTE  ZIsLTE    impossible
fromSIsLTE (SIsLTE p) = p

isItLTE : (n, m: Nat) -> Dec (n `LTE` m)
isItLTE (  Z) (  _) = Yes ZIsLTE
isItLTE (S _) (  Z) = No  absurd
isItLTE (S n) (S m) with (isItLTE n m)
  | Yes p = Yes (SIsLTE p)
  | No cp = No  (cp . fromSIsLTE)

--
-- Value-level checks
--

isZero : Nat -> Bool
isZero Z = True
isZero _ = False

isNonZero : Nat -> Bool
isNonZero = not . isZero

lt : Nat -> Nat -> Bool
lt    _    Z   = False
lt    Z  (S _) = True
lt (S n) (S m) = n `lt` m

lte : Nat -> Nat -> Bool
lte    Z     _  = True
lte (S _)    Z  = False
lte (S n) (S m) = n `lte` m

gt : Nat -> Nat -> Bool
gt = flip lt

gte : Nat -> Nat -> Bool
gte = flip lte

--
-- Utility Functions
--

||| If seen as church numerals they are the nth power of f (S)
compN : (f: a -> a) -> Nat -> (a -> a)
compN _    Z  = identity
compN f (S n) = f . (f `compN` n)

||| `fromInteger` is normally implemented as part of Algebra.Ring.Ring but Nat
||| isn't a Ring. This overloads that function.
fromInteger : Integer -> Nat
fromInteger 0 = Z
fromInteger n = -- This optimization does help!
  if n > 100 then
    assert_total ((S `compN` 100) . fromInteger $ n - 100)
  else if n > 10 then
    assert_total ((S `compN` 10) . fromInteger $ n - 10)
  else if n > 0 then
    assert_total (S . fromInteger $ n - 1)
  else
    Z

-- Unrestricted

||| (+1) Successor
succ : Nat -> Nat
succ n = (S n)

||| + (Addition)
sum : Nat -> Nat -> Nat
sum (  Z) m = m
sum (S n) m = S (n `sum` m)

||| × (Multiplication)
prod : Nat -> Nat -> Nat
prod (  Z) m = Z
prod (S n) m = m `sum` (n `prod` m)

-- Restricted

||| (-1) Predecesor
pred : (x: Nat) -> {auto ok: NonZero x} -> Nat
pred    Z  impossible
pred (S n) = n

||| - (Substraction)
minus : (x, y: Nat) -> {auto ok: x `GTE` y} -> Nat
minus (  n) (  Z) = n
minus (  Z) (S _) impossible
minus (S n) (S m) {ok=p} = minus n m {ok=fromSIsLTE p}

||| ÷ (Division) will round towards zero
div : Nat -> (y: Nat) -> {auto ok: NonZero y} -> Nat
div _ Z impossible
div Z _ = Z
div n d with (isItLTE d n) -- d > Z so minus n d < n
  | (Yes _) = S (div (assert_smaller n (minus n d)) d)
  | (No  _) = Z

||| Return the remainder of dividing
mod : Nat -> (y: Nat) -> {auto ok: NonZero y} -> Nat
mod _ Z impossible
mod Z _ = Z
mod n d with (isItLTE d n) -- d > Z so minus n d < n
  | (Yes _) = mod (assert_smaller n (minus n d)) d
  | (No  _) = n

||| Returns both the division and remainder
divMod : Nat -> (y: Nat) -> {auto ok: NonZero y} -> (Nat, Nat)
divMod n m = (div n m, mod n m)

-- Hyper-operations

||| b^p (Exponentiation)
||| @ b base
||| @ p power
pow : (b, p: Nat) -> {auto ok: Either (NonZero b) (NonZero p)} -> Nat
pow Z Z {ok=(Left  _)} impossible
pow Z Z {ok=(Right _)} impossible
pow Z _ =   Z
pow _ Z = S Z
pow n@(S _) (S m) = n `prod` (n `pow` m)

||| Ackermann-Péter-Robinson Function. Warning: Grows quickly
ackermann : Nat -> Nat -> Nat
ackermann (  Z)    m  = (S m)
ackermann (S n)    Z  = ackermann n (S Z)
ackermann (S n) (S m) = ackermann n (ackermann (S n) m)
