module Data.List.Interfaces

import Basic
import public Data.List.Base
import Data.List.Properties
import Algebra.Group.Magma
import Algebra.Group.Semigroup
import Algebra.Group.Monoid
import Category.Functor
import Category.Applicative

%default total
%access public export

Magma (List a) where
  (<>) = append

Semigroup (List a) where
  opAssoc []      _  _  = Refl
  opAssoc (x::xs) ys zs = rewrite opAssoc xs ys zs in Refl

Monoid (List a) where
  e = []
  eLeft  _  = Refl
  eRight xs = appendEmpty xs

Functor List where
  _ <$>   []    = []
  f <$> (x::xs) = f x :: (f <$> xs)

  funId   []    = Refl
  funId (x::xs) = rewrite funId xs in Refl
  funComp _ _   []    = Refl
  funComp f g (x::xs) = rewrite funComp f g xs in Refl

Applicative List where
  ( []  ) <*> _ = []
  (f::fs) <*> l = (f <$> l) <> (fs <*> l)
  pure x = [x]

  appId ( []  ) = Refl
  appId (x::xs) =
    rewrite funId xs in
    rewrite eRight xs in Refl
  appComp ( []  ) _  _ = Refl
  appComp (f::fs) [] l = rewrite appComp fs [] l in Refl
  appComp fs (g::gs) l = ?hole -- TODO
  appHomo _ _ = Refl
  appInter ( []  ) _ = Refl
  appInter (f::fs) x = rewrite appInter fs x in Refl
