module Data.List.Cast

import Basic
import public Data.Cast
import public Data.List.Base
import Data.Either.Base

--
-- Flatten a list of Eithers into Lists
--

Cast (List (Either a b)) (List a) where
  cast []              = []
  cast (Left  x :: xs) = x :: cast xs
  cast (Right _ :: xs) = cast xs

Cast (List (Either a b)) (List b) where
  cast []              = []
  cast (Left  _ :: xs) = cast xs
  cast (Right x :: xs) = x :: cast xs

Cast (List (Either a b)) (List a, List b) where
  cast l = (cast l, cast l)
