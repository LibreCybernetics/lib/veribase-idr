module Data.List.Base

import Basic
import Data.Bool.Cast
import Data.Nat.Base

%default total
%access public export

infixr 7 ::

--
-- Type
--

data List a = Nil
            | (::) a (List a)

%name List xs, ys, zs, xss, yss, zss

--
-- Type-level Checks
--

||| Type-checks iff the list is empty
data Empty : List a -> Type where
  IsEmpty : Empty ([])

Uninhabited (Empty (_::_)) where
  uninhabited IsEmpty impossible

||| Type-checks iff the list is nonempty
data NonEmpty : List a -> Type where
  IsNonEmpty : NonEmpty (_::_)

Uninhabited (NonEmpty ([])) where
  uninhabited IsNonEmpty impossible

--
-- Decidability for Type-level Checks
--

isItEmpty : (l: List a) -> Dec (Empty l)
isItEmpty []     = Yes IsEmpty
isItEmpty (_::_) = No absurd

isItNonEmpty : (l: List a) -> Dec (NonEmpty l)
isItNonEmpty []     = No absurd
isItNonEmpty (_::_) = Yes IsNonEmpty

--
-- Value-level Checks
--

isEmpty : List a -> Bool
isEmpty l = isItEmpty l & cast

isNonEmpty : List a -> Bool
isNonEmpty l = isItNonEmpty l & cast

--
-- Utility Functions
--

||| Returns the first element of a nonempty list. O(1)
head : (l: List a) -> {auto ok: NonEmpty l} -> a
head   []   impossible
head (x::_) = x

||| Returns the all the elements except the head of a nonempty list. O(1)
tail : (l: List a) -> {auto ok: NonEmpty l} -> List a
tail   []    impossible
tail (_::xs) = xs

||| Adds an element to the beginig of a list. O(1)
cons : a -> List a -> List a
cons = (::)

||| Returns a tuple with the head and tail of a nonempty list. O(1)
uncons : (l: List a) -> {auto ok: NonEmpty l} -> (a, List a)
uncons l = (head l, tail l)

||| Appends two lists together. O(n)
append : List a -> List a -> List a
append []      ys = ys
append (x::xs) ys = x :: (xs `append` ys)

||| Returns all the elements up to the second to last of a nonempty list. O(n)
init : (l: List a) -> {auto ok: NonEmpty l} -> List a
init   []           impossible
init (x::[])        = []
init (x::xs@(_::_)) = x :: init xs

||| Returns the last element of a nonempty list. O(n)
last : (l: List a) -> {auto ok: NonEmpty l} -> a
last []             impossible
last (x::[])        = x
last (x::xs@(_::_)) = last xs

||| Adds an element to the end of a list. O(n)
snoc : List a -> a -> List a
snoc xs x = xs `append` [x]

||| Returns a tuple with the init and last of a nonempty list. O(n)
unsnoc : (l: List a) -> {auto ok: NonEmpty l} -> (List a, a)
unsnoc l = (init l, last l)

||| Returns the length of a list. O(n)
length : List a -> Nat
length   []    = 0
length (x::xs) = S $ length xs
