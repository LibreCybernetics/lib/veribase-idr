module Data.List.Properties

import Builtins
import public Data.List.Base

%default total
%access export

--
-- 1
--

appendEmpty : (xs: List a) -> xs `append` [] = xs
appendEmpty []      = Refl
appendEmpty (x::xs) = rewrite appendEmpty xs in Refl
