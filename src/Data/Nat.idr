module Data.Nat

import public Data.Nat.Base
import public Data.Nat.Cast
import public Data.Nat.Interfaces
