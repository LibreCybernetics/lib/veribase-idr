||| Copyright  : LibreCybernetics
||| License    : Parity 5.0.0 or Prosperity 2.0.0
||| Maintainer : Fabián Heredia Montiel
||| Stability  : Stable
module Data.Maybe

import Basic
import Data.Bool.Cast

%default total
%access public export

--
-- Type
--

data Maybe a = Nothing
             | Just a

--
-- Type-level Checks
--

||| Type-checks iff is Nothing
data IsNothing : Maybe a -> Type where
  ItIsNothing : IsNothing (Nothing)

Uninhabited (IsNothing (Just _)) where
  uninhabited ItIsNothing impossible

||| Type-checks iff is Something
data IsJust : Maybe a -> Type where
  ItIsJust : IsJust (Just _)

Uninhabited (IsJust Nothing) where
  uninhabited ItIsJust impossible

--
-- Decidability of Type-level Checks
--

isItNothing : (m: Maybe a) -> Dec (IsNothing m)
isItNothing Nothing  = Yes ItIsNothing
isItNothing (Just _) = No absurd

isItJust : (m: Maybe a) -> Dec (IsJust m)
isItJust Nothing  = No absurd
isItJust (Just _) = Yes ItIsJust

--
-- Value-level Checks
--

isNothing : Maybe a -> Bool
isNothing m = isItNothing m & cast

isJust : Maybe a -> Bool
isJust m = isItJust m & cast

--
-- Utility Functions
--

||| Extracts a value from a Maybe
fromJust : (x: Maybe a) -> {auto ok: IsJust x} -> a
fromJust (Just x) = x
fromJust Nothing  impossible
