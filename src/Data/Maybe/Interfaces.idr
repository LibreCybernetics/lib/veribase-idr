||| Copyright  : LibreCybernetics
||| License    : Parity 5.0.0 or Prosperity 2.0.0
||| Maintainer : Fabián Heredia Montiel
||| Stability  : Stable
module Data.Maybe.Interfaces

import Basic
import public Data.Maybe.Base
import Data.Bool.Properties
import Relation.Eq
import Category.Functor
import Category.Applicative
import Category.Monad

%default total
%access public export

--
-- Relations
--

Eq a => Eq (Maybe a) where
  Nothing  == Nothing  = True
  (Just x) == (Just y) = x == y
  (     _) == (     _) = False

  eqRefl Nothing  = Refl
  eqRefl (Just x) = eqRefl x

  eqSym Nothing  Nothing  Refl = Refl
  eqSym Nothing  (Just _) Refl impossible
  eqSym (Just _) Nothing  Refl impossible
  eqSym (Just x) (Just y) p   = eqSym x y p

  eqTran Nothing  Nothing  Nothing  Refl Refl = Refl
  eqTran (     _) Nothing  (Just _) Refl Refl impossible
  eqTran (     _) (Just _) Nothing  Refl Refl impossible
  eqTran Nothing  (Just _) (     _) Refl Refl impossible
  eqTran (Just _) Nothing  (     _) Refl Refl impossible
  eqTran (Just x) (Just y) (Just z) p1  p2  = eqTran x y z p1 p2

--
-- Categorical Interfaces
--

Functor Maybe where
  _ <$> Nothing  = Nothing
  f <$> (Just x) = Just $ f x

  funId Nothing  = Refl
  funId (Just x) = Refl
  funComp _ _ Nothing  = Refl
  funComp _ _ (Just _) = Refl

Applicative Maybe where
  pure x = Just x
  Nothing  <*> _ = Nothing
  (Just f) <*> m = f <$> m

  appId m = rewrite funId m in Refl
  appComp Nothing  _        _        = Refl
  appComp (Just _) Nothing  _        = Refl
  appComp (Just _) (Just _) Nothing  = Refl
  appComp (Just _) (Just _) (Just _) = Refl
  appHomo _ _ = Refl
  appInter Nothing  _ = Refl
  appInter (Just _) _ = Refl

Monad Maybe where
  Nothing  >>= _ = Nothing
  (Just x) >>= f = f x

  monadIdLeft _ _ = Refl
  monadIdRight Nothing  = Refl
  monadIdRight (Just _) = Refl
  monadAssoc Nothing  _ _ = Refl
  monadAssoc (Just _) _ _ = Refl
