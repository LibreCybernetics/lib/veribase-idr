||| Copyright  : LibreCybernetics
||| License    : Parity 5.0.0 or Prosperity 2.0.0
||| Maintainer : Fabián Heredia Montiel
||| Stability  : Stable
module Data.Maybe.Cast

import public Data.Cast
import public Data.Maybe.Base

%default total
%access public export

||| Double Maybe Elimination
Cast (Maybe (Maybe a)) (Maybe a) where
  cast (Nothing      ) = Nothing
  cast (Just  Nothing) = Nothing
  cast (Just (Just x)) = Just x
