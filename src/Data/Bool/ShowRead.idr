||| Separated from Data.Bool.Cast to avoid a dependency cycle with Data.Either.Base
|||
||| Copyright  : LibreCybernetics 2019
||| License    : Parity 5.0.0 or Prosperity 2.0.0
||| Maintainer : Fabián Heredia Montiel
||| Stability  : Stable
module Data.Bool.ShowRead

import public Data.Bool.Base
import public Data.Cast
import public Data.Read
import public Data.Show
import Data.Either.Base

%default total
%access public export

Cast Bool String where
  cast True  = "True"
  cast False = "False"

Cast String (Either String Bool) where
  cast "t"     = Right True
  cast "T"     = Right True
  cast "f"     = Right False
  cast "F"     = Right False
  cast "true"  = Right True
  cast "True"  = Right True
  cast "TRUE"  = Right True
  cast "false" = Right False
  cast "False" = Right False
  cast "FALSE" = Right False
  cast s       = Left s
