||| Copyright  : LibreCybernetics 2019
||| License    : Parity 5.0.0 or Prosperity 2.0.0
||| Maintainer : Fabián Heredia Montiel
||| Stability  : Stable
module Data.Bool.Interfaces

import public Data.Bool.Base
import Relation.Eq
import Relation.Ord

%default total
%access public export

Eq Bool where
  True  == True  = True
  False == False = True
  _     == _     = False

  eqRefl True  = Refl
  eqRefl False = Refl

  eqSym True  True  Refl = Refl
  eqSym True  False Refl impossible
  eqSym False True  Refl impossible
  eqSym False False Refl = Refl

  eqTran True  True  True  Refl Refl = Refl
  eqTran True  True  False Refl Refl impossible
  eqTran True  False True  Refl Refl impossible
  eqTran True  False False Refl Refl impossible
  eqTran False True  True  Refl Refl impossible
  eqTran False True  False Refl Refl impossible
  eqTran False False True  Refl Refl impossible
  eqTran False False False Refl Refl = Refl

PreOrd Bool where
  True  <= True  = True
  True  <= False = False
  False <= _     = True

  ordRefl True  = Refl
  ordRefl False = Refl

  ordTran True  True  True  Refl Refl = Refl
  ordTran True  True  False Refl Refl impossible
  ordTran True  False _     Refl Refl impossible
  ordTran False True  True  Refl Refl = Refl
  ordTran False True  False Refl Refl impossible
  ordTran False False _     Refl Refl = Refl

PartialOrd Bool where
  False < True = True
  _     < _    = False

  ordAnti True  True  Refl Refl = Refl
  ordAnti True  False Refl Refl impossible
  ordAnti False True  Refl Refl impossible
  ordAnti False False Refl Refl = Refl

  strictOrd True  True  = Refl
  strictOrd True  False = Refl
  strictOrd False True  = Refl
  strictOrd False False = Refl

Ord Bool where
  ordTotal True  True  = Left  Refl
  ordTotal True  False = Right Refl
  ordTotal False True  = Left  Refl
  ordTotal False False = Left  Refl
