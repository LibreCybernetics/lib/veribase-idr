||| Copyright  : LibreCybernetics 2019
||| License    : Parity 5.0.0 or Prosperity 2.0.0
||| Maintainer : Fabián Heredia Montiel
||| Stability  : Stable
module Data.Bool.Properties

import Builtins
import public Data.Bool.Base

%default total
%access export

--
-- 1
--

||| ¬ is an involution
notInv : (x: Bool) -> not (not x) = x
notInv True  = Refl
notInv False = Refl

||| x ∨ T = T
orTrueElim : (x: Bool) -> x || True = True
orTrueElim True  = Refl
orTrueElim False = Refl

||| x ∨ F = x
orFalseElim : (x: Bool) -> x || False = x
orFalseElim True  = Refl
orFalseElim False = Refl

||| x ∨ x = x
orIdem : (x: Bool) -> x || x = x
orIdem True  = Refl
orIdem False = Refl

||| x ∧ T = x
andTrueElim : (x: Bool) -> x && True = x
andTrueElim True  = Refl
andTrueElim False = Refl

||| x ∧ F = F
andFalseElim : (x: Bool) -> x && False = False
andFalseElim True  = Refl
andFalseElim False = Refl

||| x ∧ x = x
andIdem : (x: Bool) -> x && x = x
andIdem True  = Refl
andIdem False = Refl

||| x ⊻ T = ¬x
xorTrueElim : (x: Bool) -> x `xor` True = not x
xorTrueElim True  = Refl
xorTrueElim False = Refl

||| x ⊻ F = x
xorFalseElim : (x: Bool) -> x `xor` False = x
xorFalseElim True  = Refl
xorFalseElim False = Refl

||| x ⊻ x = F
xorNilp : (x: Bool) -> x `xor` x = False
xorNilp True  = Refl
xorNilp False = Refl

--
-- 2
--

||| x ∨ y = F → x = F, y = F
falseOrExtract : (x, y: Bool) -> x || y = False -> (x = False, y = False)
falseOrExtract True  _     Refl impossible
falseOrExtract False True  Refl impossible
falseOrExtract False False Refl = (Refl, Refl)

||| x ∧ y = T → x = T, y = T
trueAndExtract : (x, y: Bool) -> x && y = True -> (x = True, y = True)
trueAndExtract True  True  Refl = (Refl, Refl)
trueAndExtract True  False Refl impossible
trueAndExtract False _     Refl impossible

||| x ∨ y = y ∨ x
orComm : (x, y: Bool) -> x || y = y || x
orComm True  y  = rewrite orTrueElim  y in Refl
orComm False y  = rewrite orFalseElim y in Refl

||| x ∧ y = y ∧ x
andComm : (x, y: Bool) -> x && y = y && x
andComm True  y = rewrite andTrueElim  y in Refl
andComm False y = rewrite andFalseElim y in Refl

||| x ⊻ y = y ⊻ x
xorComm : (x, y: Bool) -> x `xor` y = y `xor` x
xorComm True  y = rewrite xorTrueElim  y in Refl
xorComm False y = rewrite xorFalseElim y in Refl

||| ¬(x ∨ y) = ¬x ∨ ¬y
notDistOr : (x, y: Bool) -> not (x || y) = not x && not y
notDistOr True  _ = Refl
notDistOr False _ = Refl

||| ¬(x ∧ y) = ¬x ∧ ¬y
notDistAnd : (x, y: Bool) -> not (x && y) = not x || not y
notDistAnd True  _ = Refl
notDistAnd False _ = Refl

||| x ⊻ y = (x ∧ ¬y) ∨ (¬x ∧ y)
xorEquiv : (x, y: Bool) -> x `xor` y = (x && not y) || (not x && y)
xorEquiv True  y = rewrite orFalseElim (not y) in Refl
xorEquiv False _ = Refl

||| ¬(x ⊻ y) = (x ∧ y) ∨ (¬x ∧ ¬y)
notDistXor : (x, y: Bool) -> not (x `xor` y) = (x && y) || (not x && not y)
notDistXor True  y =
  rewrite orFalseElim y in
  rewrite notInv y in
  Refl
notDistXor False y = Refl

--
-- 3
--

||| x ∨ (y ∨ z) = (x ∨ y) ∨ z
orAssoc : (x, y, z: Bool) -> x || (y || z) = (x || y) || z
orAssoc True  _  _ = Refl
orAssoc False _  _ = Refl

||| x ∧ (y ∧ z) = (x ∧ y) ∧ z
andAssoc : (x, y, z: Bool) -> x && (y && z) = (x && y) && z
andAssoc True  _  _ = Refl
andAssoc False _  _ = Refl

||| x ⊻ (y ⊻ z) = (x ⊻ y) ⊻ z
xorAssoc : (x, y, z: Bool) -> x `xor` (y `xor` z) = (x `xor` y) `xor` z
xorAssoc True  True  z = rewrite notInv z in Refl
xorAssoc True  False _ = Refl
xorAssoc False _     _ = Refl

||| x ∨ (y ∧ z) = (x ∨ y) ∧ (x ∨ z)
orDistAnd : (x, y, z: Bool) -> x || (y && z) = (x || y) && (x || z)
orDistAnd True  _ _ = Refl
orDistAnd False _ _ = Refl

||| x ∧ (y ∨ z) = (x ∧ y) ∨ (x ∧ z)
andDistOr : (x, y, z: Bool) -> x && (y || z) = (x && y) || (x && z)
andDistOr True  _ _ = Refl
andDistOr False _ _ = Refl
