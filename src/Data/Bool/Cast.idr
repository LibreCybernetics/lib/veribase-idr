||| Copyright  : LibreCybernetics 2019
||| License    : Parity 5.0.0 or Prosperity 2.0.0
||| Maintainer : Fabián Heredia Montiel
||| Stability  : Stable
module Data.Bool.Cast

import Basic
import public Data.Cast
import public Data.Bool.Base

%default total
%access public export

--
-- Primitives
--

||| Will take 0 as absence and anything else, postive or negative, as presence.
Cast Int Bool where
  cast 0 = False
  cast _ = True

||| Will take 0 as absence and anything else, postive or negative, as presence.
Cast Integer Bool where
  cast 0 = False
  cast _ = True

||| Will take 0 as absence and anything else, postive or negative, as presence.
Cast Double Bool where
  cast 0.0 = False
  cast _   = True

--
-- Basics
--

||| Forgets the proof or counterproof
Cast (Dec a) Bool where
  cast (Yes _) = True
  cast (No  _) = False
