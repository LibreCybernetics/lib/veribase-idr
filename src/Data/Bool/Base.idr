||| Copyright  : LibreCybernetics 2019
||| License    : Parity 5.0.0 or Prosperity 2.0.0
||| Maintainer : Fabián Heredia Montiel
||| Stability  : Stable
module Data.Bool.Base

import Builtins

%default total
%access public export

infixr 5 ||
infixr 4 &&

--
-- Type
--

data Bool = True | False

--
-- Utility Functions
--

||| ¬ (Logical Negation)
not : Bool -> Bool
not True  = False
not False = True

||| ∨ (Logical Disjunction)
(||) : Bool -> Bool -> Bool
True  || _ = True
False || y = y

||| ∧ (Logical Conjunction)
(&&) : Bool -> Bool -> Bool
True  && y = y
False && _ = False

||| ⊻ (Logical Exclusive Disjunction)
xor : Bool -> Bool -> Bool
xor True  y = not y
xor False y = y

||| Needed for syntactic support of `if p then code₁ else code₂`
ifThenElse : Bool -> Lazy a -> Lazy a -> a
ifThenElse True  t _ = t
ifThenElse False _ e = e
