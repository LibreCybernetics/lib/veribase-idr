# VeriBase

A verified base package and alternative prelude inspired by Haskell's Base
Library (BSD-3 License), Stephen Diehl's Protolude (MIT License), Idris'
Prelude and Base (BSD-3 License), and Agda's Standard Library (MIT License).

All functions are total and include type-level checks for correct usage, for
example: `head` will not type check unless it can be proven at compile time that
the list it is passed will always be NonEmpty.

The proofs are derived automatically if it is syntactically clear by the
magnificent type checker of Idris developed by Edwin Brady and the Idris
Community, but the developers will continue to carry the burden of doing the
case analysis if it isn't clearcut; the type checker will make sure this need
doesn't go unnoticed.

The properties aren't exported automatically, everything else is exposed by
importing Protolude.

This is still in development, and regardless of the verification mentioned, this
library comes without warranties per the licenses. The use of "verified" depends
on trusting Idris and everything in the machine it is used alongside with.

## License

This work is dual licensed between a radical copyleft license and a
noncommercial one. If you want use this library it should be either for libre
open source development or non-comercial use. (Parity 5.0.0 or Prosperity 2.0.0)

A copy of both licenses comes with this repo, however some longer explanations
might be needed for these unusual licenses:

 - https://blog.licensezero.com/2018/11/24/parity-5.0.0.html
 - https://blog.licensezero.com/2018/10/26/no-other-terms.html
 - https://blog.licensezero.com/2018/09/16/two-party.html
 - https://blog.licensezero.com/2018/09/14/free-to-take-freedom.html
 
Some further reading:

 - http://wiki.p2pfoundation.net/Copyfair
 - http://wiki.p2pfoundation.net/Copyfarleft
 - http://wiki.p2pfoundation.net/Peer_Production_License
 
This library cannot be used by commercial privative software. (And would
encourage doubt on any "verified" privative software)

Please note that the included public licenses does not include any commitment to
fix bugs, add features, maintain compatibility, provide support, or perform any
other services, whatsoever.

## Contributing

Since the licenses are new and different, relicensing to a new improved version
or to other strong copyleft licenses is not out of the question. By contributing
trough a patch or merge request to this project you have to explicitly either
license your contribution under a permissive license like Unlicense or to
transfer the copyright to LibreCybernetics.
